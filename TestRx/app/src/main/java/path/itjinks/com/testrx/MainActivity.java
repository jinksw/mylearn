package path.itjinks.com.testrx;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import java.util.concurrent.TimeUnit;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Observable.error(new RuntimeException("fuck"))
        .onErrorResumeNext(Observable.error(new RuntimeException("haha")))
        .subscribe(new Action1<Object>() {
          @Override public void call(Object o) {
            System.out.println(o.toString());
          }
        }, new Action1<Throwable>() {
          @Override public void call(Throwable throwable) {
            System.out.println(throwable.toString());
          }
        });

    Observable.merge(Observable.just(1, 2), Observable.error(new RuntimeException("hehe")),
        Observable.just(3, 4)).onErrorResumeNext(new Func1<Throwable, Observable<?>>() {
      @Override public Observable<?> call(Throwable throwable) {
        return Observable.just(5, 6);
      }
    }).subscribe(new Action1<Object>() {
      @Override public void call(Object o) {
        System.out.println(o.toString());
      }
    }, new Action1<Throwable>() {
      @Override public void call(Throwable throwable) {
        System.out.println(throwable.toString());
      }
    });
  }

  private Subscription mJobSubscription;

  public void dojob(View view) {
    if (mJobSubscription == null) {
      mJobSubscription = Observable.create(new Observable.OnSubscribe<Void>() {
        @Override public void call(Subscriber<? super Void> subscriber) {
          try {
            Thread.sleep(5000);
            subscriber.onNext(null);
            subscriber.onCompleted();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      })
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Action1<Void>() {
            @Override public void call(Void aVoid) {
              System.out.println("job done");
              mJobSubscription = null;
            }
          });
    }
  }

  public void canceljob(View view) {
    if (mJobSubscription != null) {
      mJobSubscription.unsubscribe();
      mJobSubscription = null;
    }
  }

  public void reduce(View view) {
    Observable.just(1, 2, 3, 4, 5).reduce(new Func2<Integer, Integer, Integer>() {
      @Override public Integer call(Integer integer, Integer integer2) {
        return integer * integer2;
      }
    }).subscribe(new Action1<Integer>() {
      @Override public void call(Integer integer) {
        System.out.println("value " + integer);
      }
    });
  }

  public void merge(View view) {
    Observable.merge(Observable.just(1, 2, 3, 4), Observable.just(5, 6, 7, 8))
        .subscribe(new Action1<Integer>() {
          @Override public void call(Integer integer) {
            System.out.println("value " + integer);
          }
        });
  }

  public void zip(View view) {
    Observable.zip(Observable.just(1, 2, 3, 4), Observable.just(5, 6, 7, 8),
        new Func2<Integer, Integer, Integer>() {
          @Override public Integer call(Integer integer, Integer integer2) {
            return integer + integer2;
          }
        }).subscribe(new Action1<Integer>() {
      @Override public void call(Integer integer) {
        System.out.println("value " + integer);
      }
    });
  }

  Subscription mSubscription;

  public void start(View view) {
    if (mSubscription != null && !mSubscription.isUnsubscribed()) {
      return;
    }
    mSubscription = Observable.timer(5, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Action1<Long>() {
          @Override public void call(Long aLong) {
            Toast.makeText(getApplicationContext(), "time out", Toast.LENGTH_LONG).show();
          }
        });
  }

  public void stop(View view) {
    if (mSubscription != null && !mSubscription.isUnsubscribed()) {
      Toast.makeText(getApplicationContext(), "cancel", Toast.LENGTH_LONG).show();
      mSubscription.unsubscribe();
    }
  }
}
